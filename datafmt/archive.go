package datafmt

import (
	"time"

	"github.com/bwmarrin/discordgo"
)

type Message struct {
	ID        string `json:"id,omitempty"`
	ChannelID string `json:"channel_id,omitempty"`
	GuildID   string `json:"guild_id,omitempty"`
	Content   string `json:"content,omitempty"`

	Timestamp       time.Time `json:"timestamp,omitempty"`
	EditedTimestamp time.Time `json:"edited_timestamp,omitempty"`

	TTS    bool `json:"tts"`
	Pinned bool `json:"pinned"`

	MentionRoles    []string  `json:"mention_roles,omitempty"`
	MentionEveryone bool      `json:"mention_everyone"`
	MentionUsers    []User    `json:"mention_users,omitempty"`
	MentionChannels []Channel `json:"mention_channels,omitempty"`

	Author User `json:"author,omitempty"`

	Attachments []discordgo.MessageAttachment `json:"attachments,omitempty"`
	Embeds      []discordgo.MessageEmbed      `json:"embeds,omitempty"`
	Reactions   []discordgo.MessageReactions  `json:"reactions,omitempty"`

	Type discordgo.MessageType `json:"type"`

	WebhookID string `json:"webhook_id,omitempty"`

	Activity discordgo.MessageActivity `json:"activity,omitempty"`

	Application discordgo.MessageApplication `json:"application,omitempty"`

	MessageReference discordgo.MessageReference `json:"message_reference,omitempty"`

	Flags discordgo.MessageFlags `json:"flags"`
}

type User struct {
	ID            string `json:"id"`
	Username      string `json:"username"`
	Avatar        string `json:"avatar"`
	Discriminator string `json:"discriminator"`
	Bot           bool   `json:"bot"`
}

type Channel struct {
	ID               string                `json:"id"`
	GuildID          string                `json:"guild_id,omitempty"`
	Name             string                `json:"name"`
	Topic            string                `json:"topic"`
	Type             discordgo.ChannelType `json:"type"`
	NSFW             bool                  `json:"nsfw"`
	ParentID         string                `json:"parent_id"`
	RateLimitPerUser int                   `json:"rate_limit_per_user"`
}

type ArchiveData struct {
	PackVersion int       `json:"packVersion"`
	Date        time.Time `json:"timestamp"`

	Messages []Message `json:"messages"`
	Channel  Channel   `json:"channel"`
}
